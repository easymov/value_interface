# value_interface

## Overview

This package contains a `ros_control` interface to manage any kind of value.

This is useful to declare values in your `ros_control` drivers that can be used in loaded controllers.

`ValueStateHandle<Value>` allows read operation on the managed value while `ValueHandle<Value>` allows read and write operations.

Controllers can use `ValueStateInterface<Value>` and `ValueInterface<Value>` to get access to managed values.

`ValueStateInterface<Value>` doesn't claim the underlaying resource while `ValueInterface<Value>` does.

## Usage

### Driver side

```cpp
double analog_output_1_value;
double analog_output_1_command;

value_interface::ValueHandle<double> analog_output_handle(
    "analog_output_1",
    &analog_output_1_value,
    &analog_output_1_command);

write_this_value_to_my_analog_output(analog_output_1_command);
analog_output_1_value = read_the_value_of_my_analog_output();
```

### Controller side

```cpp
void Controller::init(
    value_interface::ValueInterface<double>* hw, ros::NodeHandle& nh)
{
    analog_output_handle = hw->getHandle("analog_output_1");
    analog_output_handle.set(5.0);
    double analog_value = analog_output_handle.get();
}
```