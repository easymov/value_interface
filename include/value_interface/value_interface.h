/**
 *  @file value_interface.h
 *  @author Easymov Robotics
 *  @date 2018-07
 */

#ifndef VALUE_INTERFACE_H
#define VALUE_INTERFACE_H

#include <map>
#include <stdexcept>
#include <string>

#include <hardware_interface/internal/hardware_resource_manager.h>

namespace value_interface
{
/**
 * This class store the address of a Value to be set by a ros_control
 * controller.
 */
template <typename Value>
class ValueStateHandle
{
public:
  /**
   * Default constructor.
   *
   * @param value_ptr_ is the address of the managed Value
   */
  ValueStateHandle(const std::string& name = "", Value* value_ptr = nullptr) : name_(name), value_ptr_(value_ptr)
  {
  }

  /**
   * Get the value of the managed Value.
   *
   * @param value is the new value
   */
  Value get() const
  {
    if (value_ptr_ == nullptr)
    {
      throw std::runtime_error(std::string("getting value from null ") + name_ + " handle");
    }

    return *value_ptr_;
  }

  std::string getName() const
  {
    return name_;
  }

protected:
  std::string name_;
  Value* value_ptr_;
};

/**
 * This class store the address of a Value to be set by a ros_control
 * controller.
 */
template <typename Value>
class ValueHandle : public ValueStateHandle<Value>
{
public:
  /**
   * Default constructor.
   *
   * @param value_ptr_ is the address of the managed Value
   */
  ValueHandle(const std::string& name = "", Value* value_ptr = nullptr, Value* command_ptr = nullptr)
    : ValueStateHandle<Value>(name, value_ptr), command_ptr_(command_ptr)
  {
  }

  /**
   * Set the value of the managed Value.
   *
   * @param value is the new value
   */
  void set(const Value& value)
  {
    if (command_ptr_ == nullptr)
    {
      throw std::runtime_error(std::string("setting value to null ") + ValueStateHandle<Value>::name_ + " handle");
    }

    *command_ptr_ = value;
  }

protected:
  Value* command_ptr_;
};

template <typename Value>
class ValueStateInterface
  : public hardware_interface::HardwareResourceManager<ValueStateHandle<Value>, hardware_interface::DontClaimResources>
{
};

template <typename Value>
class ValueInterface
  : public hardware_interface::HardwareResourceManager<ValueHandle<Value>, hardware_interface::ClaimResources>
{
};

}  // namespace value_interface

#endif
